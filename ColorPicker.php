<?php

/**
 * ColorPicker
 *
 * @author Bogdan Savluk <savluk.bogdan@gmail.com>
 */


class ColorPicker extends CInputWidget
{
    /** @var string Path to assets directory published in init() */
    private $assetsDir;

    private $settings = array();

    /** Publish assets and set default values for properties */
    public function init()
    {
        $dir = dirname(__FILE__) . '/colorpicker';
        $this->assetsDir = Yii::app()->assetManager->publish($dir);
    }

    /** Render widget html and register client scripts */
    public function run()
    {
        list($name, $id) = $this->resolveNameID();
        if (isset($this->htmlOptions['id']))
            $id = $this->htmlOptions['id'];
        else
            $this->htmlOptions['id'] = $id;
        if (isset($this->htmlOptions['name']))
            $name = $this->htmlOptions['name'];

        if (isset($this->model)) {
            //. "<input type=\"text\" class=\"span3\" value=\"\" readonly >"
            echo CHtml::textField($name, CHtml::resolveValue($this->model, $this->attribute), $this->htmlOptions);
        } else {
            echo CHtml::textField($name, $this->value, $this->htmlOptions);
        }
        $this->registerScripts($id);
    }

    /** Register client scripts */
    private function registerScripts($id)
    {
        $cs = Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery');
        if (defined('YII_DEBUG'))
            $cs->registerScriptFile($this->assetsDir . '/js/bootstrap-colorpicker.js');
        else
            $cs->registerScriptFile($this->assetsDir . '/js/bootstrap-colorpicker.min.js');

        $cs->registerCssFile($this->assetsDir . '/css/bootstrap-colorpicker.css');

        $settings = CJavaScript::encode($this->settings);
        $cs->registerScript("{$id}_colorpicker", "$('#{$id}').colorpicker({$settings});");
    }
}
